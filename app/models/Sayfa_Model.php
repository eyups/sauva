<?php
/**
 * Created by PhpStorm.
 * User: EyüpSabri
 * Date: 14.6.2015
 * Time: 13:15
 */

class Sayfa_Model extends Model {

    public function __construct() {
        parent::__construct();
    }

    public function pageControl($array = array())
    {
        $sql = "SELECT * FROM ". T_SAYFALAR ." WHERE short_name = :short_name";

        $count = $this->db->affectedRows($sql, $array);

        if($count > 0){
            //Sayfa ismi sistemde mevcut.
            $sql = "SELECT * FROM ". T_SAYFALAR ." WHERE short_name = :short_name";
            return $this->db->select($sql,$array);
        }else{
            return false;
        }
    }

    public function menuGetir(){
        $sql = "SELECT * FROM ". T_SAYFALAR ." ORDER BY rank ASC";
        return $this->db->select($sql);
    }
} 