
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">

            <li>
                <a href="#"><i class="fa fa-image"></i> Anasayfa Kayan Resim<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="<?php echo SITE_URL; ?>/Panel/anasayfaMedyaEkle">Yeni Resim Ekle</a>
                    </li>
                    <li>
                        <a href="<?php echo SITE_URL; ?>/Panel/anasayfaMedyaList">Resimleri Görüntüle</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>

            <li>
                <a href="#"><i class="fa fa-copy"></i> Sayfalar<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="<?php echo SITE_URL; ?>/Panel/sayfaEkle">Yeni Sayfa Ekle</a>
                    </li>
                    <li>
                        <a href="<?php echo SITE_URL; ?>/Panel/sayfaList">Sayfaları Görüntüle</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>

            <li>
                <a href="#"><i class="fa fa-bullhorn"></i> Duyurular<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="<?php echo SITE_URL; ?>/Panel/duyuruEkle">Duyuru Ekle</a>
                    </li>
                    <li>
                        <a href="<?php echo SITE_URL; ?>/Panel/duyuruList">Duyuruları Göster</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>

            <li>
                <a href="#"><i class="fa fa-tags"></i> Kampanyalar<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="<?php echo SITE_URL; ?>/Panel/kampanyaEkle">Kampanya Ekle</a>
                    </li>
                    <li>
                        <a href="<?php echo SITE_URL; ?>/Panel/kampanyaList">Kampanyaları Göster</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>

            <li>
                <a href="#"><i class="fa fa-stack-exchange"></i> Broşürler<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="<?php echo SITE_URL; ?>/Panel/brosurEkle">Broşür Ekle</a>
                    </li>
                    <li>
                        <a href="<?php echo SITE_URL; ?>/Panel/brosurList">Broşürleri Göster</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>

            <li>
                <a href="<?php echo SITE_URL; ?>/Panel/siteSettings"><i class="fa fa-gears"></i> Site Ayarları</a>
            </li>


            <!-- /.nav-second-level -->
            </li>

        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
</nav>