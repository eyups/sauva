<section class="post-wrapper-top jt-shadow clearfix">
    <div class="container">
        <div class="col-lg-12">
            <h2>Duyurular</h2>
            <ul class="breadcrumb pull-right">
                <li><a href="#">Kurumsal</a></li>
                <li>Duyurular</li>
            </ul>
        </div>
    </div>
</section><!-- end post-wrapper-top -->

<section class="blog-wrapper">
    <div class="container">
        <div id="content" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <ul class="timeline">
                <?php
                        foreach($duyuruListe as $key => $value){
                            if(($key+1)%2 == 1){
                                echo '<li class="wow fadeInLeft" data-wow-offset="100">
                                        <div class="timeline-badge danger"><i class="fa fa-newspaper-o"></i></div>
                                            <div class="timeline-panel">
                                                <div class="blog-carousel">
                                                    <div class="entry">
                                                        <img src="' . SITE_PUBLIC . $value["url"] . '" alt="" class="img-responsive">
                                                        <div class="magnifier">
                                                            <div class="buttons">
                                                                <a class="st" rel="bookmark" href="' . SITE_URL . "/Duyuru/Detay/" . $value["id"] . '"><i class="fa fa-link"></i></a>
                                                            </div><!-- end buttons -->
                                                        </div><!-- end magnifier -->
                                                        <div class="post-type">
                                                            <i class="fa fa-picture-o"></i>
                                        </div><!-- end pull-right -->
                                                    </div><!-- end entry -->
                                    <div class="blog-carousel-header">
                                        <h3><a title="" href="' . SITE_URL . "/Duyuru/Detay/" . $value["id"] . '">' . str_replace(array(".", "!"),"",mb_strtoupper($value["title"])) . '</a></h3>
                                        <div class="blog-carousel-meta">
                                            <span><i class="fa fa-calendar"></i> '. date('d/m/y', strtotime($value["date"])) .'</span>
                                        </div><!-- end blog-carousel-meta -->
                                    </div><!-- end blog-carousel-header -->
                                    <div class="blog-carousel-desc">
                                        <p>'. $value["short_content"] .'</p>
                                    </div><!-- end blog-carousel-desc -->
                                </div><!-- end blog-carousel -->
                            </div>
                        </li>';
                            }else {
                                echo '<li class="timeline-inverted wow fadeInRight" data-wow-offset="100">
                            <div class="timeline-badge danger"><i class="fa fa-newspaper-o"></i></div>
                            <div class="timeline-panel">
                                <div class="blog-carousel">
                                    <div class="entry">
                                        <img src="' . SITE_PUBLIC . $value["url"] . '" alt="" class="img-responsive">
                                        <div class="magnifier">
                                            <div class="buttons">
                                                <a class="st" rel="bookmark" href="' . SITE_URL . "/Duyuru/Detay/" . $value["id"] . '"><i class="fa fa-link"></i></a>
                                            </div><!-- end buttons -->
                                        </div><!-- end magnifier -->
                                        <div class="post-type">
                                            <i class="fa fa-picture-o"></i>
                                        </div><!-- end pull-right -->
                                    </div><!-- end entry -->
                                    <div class="blog-carousel-header">
                                        <h3><a title="" href="' . SITE_URL . "/Duyuru/Detay/" . $value["id"] . '">' . str_replace(array(".", "!"),"",mb_strtoupper($value["title"])) . '</a></h3>
                                        <div class="blog-carousel-meta">
                                            <span><i class="fa fa-calendar"></i> '. date('d/m/Y', strtotime($value["date"])) .'</span>
                                        </div><!-- end blog-carousel-meta -->
                                    </div><!-- end blog-carousel-header -->
                                    <div class="blog-carousel-desc">
                                        <p>'. $value["short_content"] .'</p>
                                    </div><!-- end blog-carousel-desc -->
                                </div><!-- end blog-carousel -->
                            </div>
                        </li>';
                    }
                }
                ?>



            </ul>

            <div class="clearfix"></div>

            <hr>

            <div class="pagination_wrapper text-center">
                <!-- Pagination Normal -->
                <ul class="pagination">
                    <?php
                        if($sayfaNo != 1)
                            echo '<li><a href="' . SITE_URL . "/Duyuru/" . (intval($sayfaNo) - 1) . '">«</a></li>';
                        else
                            echo '<li><a href="#">«</a></li>';
                        for($i = 1; $i <= $toplamSayfa; $i++) {
                            if($sayfaNo == $i) { // Eğer Bulunduğumuz Sayfada ise farklı kod ve link yok
                                echo '<li class="active"><a href="#">' . $i . '</a></li>';
                            } else {
                                echo '<li><a href="' . SITE_URL . "/Duyuru/" . $i . '">' . $i . '</a></li>';
                            }
                        }
                        if($sayfaNo == $toplamSayfa)
                            echo '<li><a href="#">»</a></li>';
                        else
                            echo '<li><a href="' . SITE_URL . "/Duyuru/" . (intval($sayfaNo) + 1) . '">»</a></li>';

                    ?>
                </ul>
            </div><!-- end pagination_wrapper -->
        </div><!-- end content -->
    </div><!-- end container -->
</section><!--end white-wrapper -->
