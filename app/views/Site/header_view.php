<!DOCTYPE html>
<html lang="tr">
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php
            if($sayfaBaslik == "Anasayfa") {
            echo "<title>". $site_title ." - Hoşgeldiniz </title>";
        }else{
            echo "<title>". $sayfaBaslik . " - " .  $site_title ." | hadim.com.tr</title>";
        }
    ?>


    <link rel="alternate" hreflang="tr" href="http://www.hadim.com.tr/" />

    <link rel="shortcut icon" type="image/png" href="<?php echo SITE_PUBLIC; ?>/images/favicon.png"/>

    <script>

    function guncelIndirim(){
       $.scrollTo($("#indirimler"), 1500, {offset: {top:-100}});
        var html = "<p style='color: black;'>Güncel indirimlerimizi görmenizi istedik! Bizi takipte kalın, indirimleri kaçırmayın! <br>Sitenin en başına gitmek için 'Başa Dön', bu mesajı kapatmak için 'Kapat' tuşlarını kullanabilirsiniz.</p>";
        new $.flavr({
            content         : html,
            position         : 'top-mid',
            closeEsc		: true,
            closeOverlay	: true,
            animateEntrance : 'fadeInUpBig',
            animateClosing  : 'fadeOutRightBig',
            buttons         : {
              basadon  : { text: 'Başa Dön', style: 'danger', addClass: 'submit-btn',
                action: function( $container, $second, $third ){
                    $.scrollTo($("#topbar"), 1500);
                }
              },
                kapat       : {}
            }

        });

    }
    </script>


    <!-- Support for HTML5 -->
    <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Enable media queries on older bgeneral_rowsers -->
    <!--[if lt IE 9]>
    <script src="<?php echo SITE_PUBLIC; ?>/js/respond.min.js"></script>  <![endif]-->

</head>
<body>


<div id="topbar" class="clearfix">
    <div class="container">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="social-icons">
                <span><a data-toggle="tooltip" data-placement="bottom" title="Facebook" target="_blank" href="<?php echo $facebook ?>"><i class="fa fa-facebook"></i></a></span>
                <span class="last"><a data-toggle="tooltip" data-placement="bottom" title="Twitter" target="_blank" href="<?php echo $twitter ?>"><i class="fa fa-twitter"></i></a></span>
            </div><!-- end social icons -->
        </div><!-- end columns -->
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
            <div class="callus">

                <span class="topbar-phone"><i class="fa fa-phone"></i> <?php echo $phone ?></span>
            </div><!-- end callus -->
        </div><!-- end columns -->
    </div><!-- end container -->
</div><!-- end topbar -->

<header id="header-style-1">
<div class="container">
<div class="navbar yamm navbar-default">
<div class="navbar-header">
    <button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <a href="<?php echo SITE_URL; ?>/Index" class="navbar-brand"></a>
</div><!-- end navbar-header -->

<div id="navbar-collapse-1" class="navbar-collapse collapse navbar-right">
<ul class="nav navbar-nav">
    <?php

    // İçiçe (Nested) Sayfa Yazdırma Sağlayan Rekürsif Fonksiyonumuz
    function sayfa_nested($sayfalar , $ebeveyn = 0  , $kademe_pixel = 5 ,  $i = 0  ,  $menuler = NULL , $nested = FALSE )
    {

        // Sayfalar Boşşa boş döndür.
        if(empty($sayfalar))
            return;


        // Eğer fonksiyon içinden çağırılmıyorsa
        if (!$nested)
        {
            // Sayfaları ebeveyn idsi ile yeni dizi oluştur
            foreach($sayfalar as $row):
                $items[$row['ust_id']][] = $row;
            endforeach;
        }
        else
        {
            // Nested ise gelen sayfaları al
            $items  = $sayfalar;
        }

        // Gelen sayfaları aç
        foreach($items[$ebeveyn] as $sayfa)
        {
            // Boşluk hesapla
            $bosluk = str_repeat('&nbsp;',($i*$kademe_pixel));

            // Açılan menude bir alt sayfa var ise nested çağır
            if($sayfa['anamenu_visible']){
                if (isset($items[$sayfa['id']]))
                {
                    $menuler    .= '<li class="dropdown"><a data-toggle="dropdown" class="dropdown-toggle" href="' . SITE_URL . '/Sayfa/' . $sayfa['short_name'] . '">' . $sayfa['name'] . "<div class='arrow-up'></div></a>" .PHP_EOL;
                    $menuler .= '<ul class="dropdown-menu" role="menu">'.PHP_EOL;
                    $menuler = sayfa_nested($items , $sayfa['id']  , $kademe_pixel , ($i+1) , $menuler , TRUE);
                    $menuler .= '</ul>'.PHP_EOL;
                }else{
                    if($sayfa["direk_link"] == null) {
                        $menuler    .= '<li><a href="' . SITE_URL . '/Sayfa/' . $sayfa['short_name'] . '">' . $sayfa['name'] . "</a>";
                        $menuler    .= '</li>'.PHP_EOL;
                    }else{
                        $menuler    .= '<li><a href="' . SITE_URL . '/'. $sayfa["direk_link"] .  '">' . $sayfa['name'] . "</a>";
                        $menuler    .= '</li>'.PHP_EOL;
                    }

                }

            }

        }

        // Oluşan menüleri return et
        return $menuler;
    }


    echo sayfa_nested($menuListe);


    ?>
</ul><!-- end navbar-nav -->
</div><!-- #navbar-collapse-1 -->
</div><!-- end navbar yamm navbar-default -->
</div><!-- end container -->
</header><!-- end header-style-1 -->
