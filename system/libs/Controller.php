<?php

class Controller {
    protected $load = array();
    protected $ayarModel = array();
    protected $data = array();

    public function __construct() {
        $this->load = new Load();
        $this->ayarModel = new Model();
        $temp = $this->ayarModel->icerikListele(TABLO_ONEKI . "settings");

        // mb_strtoupper in Turkce karakterlerle dogru calismasi icin eklendi.
        mb_internal_encoding("UTF-8");

        $this->data["footer_text"] =  $temp[5]["val"];
        $this->data["phone"] =  $temp[4]["val"];
        $this->data["email"] =  $temp[3]["val"];
        $this->data["site_title"] =  $temp[2]["val"];
        $this->data["twitter"] =  $temp[1]["val"];
        $this->data["facebook"] =  $temp[0]["val"];


    }

    public function ipGetir()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress;
    }

}
